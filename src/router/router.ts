import Home from "@/views/Home.vue";
import Dce1_1 from "@/views/Dce1_1.vue";
import Dce1_2 from "@/views/Dce1_2.vue";
import Dce1_3 from "@/views/Dce1_3.vue";
import Dce1_4 from "@/views/Dce1_4.vue";
import Dce1_5 from "@/views/Dce1_5.vue";
import Dce1_6 from "@/views/Dce1_6.vue";
import Dce1_7 from "@/views/Dce1_7.vue";
import Dce1_8 from "@/views/Dce1_8.vue";
import Dce1_9 from "@/views/Dce1_9.vue";
import Dce1_10 from "@/views/Dce1_10.vue";
import Dce1_11 from "@/views/Dce1_11.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/Dce1_1",
    name: "1.1.	一日流程展示",
    component: Dce1_1
  },
  {
    path: "/Dce1_2",
    name: "1.2.	清算7.0告警信息",
    component: Dce1_2
  },
  {
    path: "/Dce1_3",
    name: "1.3.	cpu、内存使用率",
    component: Dce1_3
  },
  {
    path: "/Dce1_4",
    name: "1.4.	Kafka topic消费状态",
    component: Dce1_4
  },
  {
    path: "/Dce1_5",
    name: "1.5.	出入金监控 ",
    component: Dce1_5
  },
  {
    path: "/Dce1_6",
    name: "1.6.	数据库写入状态",
    component: Dce1_6
  },
  {
    path: "/Dce1_7",
    name: "1.7. 内存使用率查询",
    component: Dce1_7
  },
  {
    path: "/Dce1_8",
    name: "1.8.	结算流程查询",
    component: Dce1_8
  },
  {
    path: "/Dce1_9",
    name: "1.9.	告警信息查询",
    component: Dce1_9
  },
  {
    path: "/Dce1_10",
    name: "1.10.	kafka消费状况历史查询",
    component: Dce1_10
  },
  {
    path: "/Dce1_11",
    name: "1.11.	进程运行状态",
    component: Dce1_11
  }
];

export default routes;
